package de.drentech.bokasafn.bookservice.common.health;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;

import javax.enterprise.context.ApplicationScoped;

@Liveness
@ApplicationScoped
public class ApplicationHealthCheck implements HealthCheck {

    @ConfigProperty(name = "application.up", defaultValue = "false")
    private boolean applicationUp;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder responseBuilder = HealthCheckResponse.named("application-check");

        if(applicationUp) {
            return responseBuilder.up().build();
        } else {
            return responseBuilder.down().build();
        }
    }
}
